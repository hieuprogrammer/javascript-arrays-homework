/* BÀI TẬP 1 */
document.getElementById(`btn-bai-tap-1`).addEventListener(`click`, function() {
    var array = [];
    var sum = 0;
    var max = document.getElementById(`input-number-1`).value;

    for (var index = 1; index <= max; index++) {
        array.push(index);
        sum += index;
    }

    document.getElementById(`ket-qua-bai-tap-1`).innerHTML = `Total of all positive intergers from 0 to ${max}: ${sum}.`;
});

/* BÀI TẬP 2 */
document.getElementById(`btn-bai-tap-2`).addEventListener(`click`, function() {
    var array = [];
    var evenValuesFound = [];
    var max = document.getElementById(`input-number-2`).value;

    for (var index = Number.MIN_VALUE; index <= max; index++) {
        array.push(index);

        if (index > 0 && index % 2 === 0) {
            evenValuesFound.push(index);
        }
    }

    document.getElementById(`ket-qua-bai-tap-2`).innerHTML = `Total of all positive intergers found: ${evenValuesFound.length}, they are: ${evenValuesFound.join(`, `)}.`;
});

/* BÀI TẬP 3 */
document.getElementById(`btn-bai-tap-3`).addEventListener(`click`, function() {
    var array = [];
    var min = Number.MAX_VALUE;    

    var number = document.getElementById(`input-number-3`).value;
    for (var index = 0; index < number; index++) {
        array.push(index);
        if (array[index] < min) {
            min = array[index];
        }
    }

    document.getElementById(`ket-qua-bai-tap-3`).innerHTML = `Smallest value in numbers array: ${min}.`;
});

/* BÀI TẬP 4 */
document.getElementById(`btn-bai-tap-4`).addEventListener(`click`, function() {
    var array = [];
    var min = Number.MAX_VALUE;    

    var number = document.getElementById(`input-number-4`).value;
    for (var index = 0; index < number; index++) {
        array.push(index);
        if (array[index] > 0 && array[index] < min) {
            min = array[index];
        }
    }

    document.getElementById(`ket-qua-bai-tap-4`).innerHTML = `Smallest positive value in numbers array: ${min}.`;
});

/* BÀI TẬP 5 */
document.getElementById(`btn-bai-tap-5`).addEventListener(`click`, function() {
    var array = [];
    var lastEvenElement;   

    var number = document.getElementById(`input-number-5`).value;
    for (var index = 0; index < number; index++) {
        array.push(index);
    }

    for (var index = array.length - 1; index > 0; index--) {
        if (array[index] % 2 === 0) {
            lastEvenElement = array[index];
            break;
        } else {
            lastEvenElement = -1;
            break;
        }
    }

    document.getElementById(`ket-qua-bai-tap-5`).innerHTML = `Smallest positive value in numbers array: ${lastEvenElement}.`;
});

/* BÀI TẬP 6 */
document.getElementById(`btn-bai-tap-6`).addEventListener(`click`, function() {
    var array = [];

    var number = document.getElementById(`input-number-6`).value;
    for (index = 0; index < number; index++) {
        array.push(index);
    }

    document.getElementById(`generated-array`).innerHTML = `<b>Before</b>: ${array}.`;

    var rootIndex = document.getElementById(`root-index`).value;
    var destinationIndex = document.getElementById(`destination-index`).value;
    var temp = 0;

    temp = array[rootIndex];
    array[rootIndex] = array[destinationIndex];
    array[destinationIndex] = temp;

    document.getElementById(`ket-qua-bai-tap-6`).innerHTML = `<b>After</b>: ${array}.`;
});

/* BÀI TẬP 7 */
function sortAscending(array) {
    var temp = array[0];
    for (var i = 0 ; i < array.length - 1; i++) {
        for (var j = i + 1; j < array.length; j++) {
            if (array[i] > array[j]) {
                temp = array[j];
                array[j] = array[i];
                array[i] = temp;
            }
        }
    }
}

document.getElementById(`btn-bai-tap-7`).addEventListener(`click`, function() {
    var array = [];
    var innerContent = ``;

    var number = document.getElementById(`input-number-7`).value;
    for (index = number; index > 0; index--) {
        array.push(index);
    }

    innerContent += `<b>Array</b>: ${array}.<br/>`;

    sortAscending(array);

    document.getElementById(`ket-qua-bai-tap-7`).innerHTML = innerContent += `<b>Sorted array</b>: ${array}.`;
});

/* BÀI TẬP 8 */
function isPrime(number) {
    var count = 0;
    for (var index = 1; index < number; index++) {
        if (number) {
            number % index === 0 ? count++ : count;
        }
    }

    if (count === 2) {
        return true;
    } else {
        return false;
    }
}

document.getElementById(`btn-bai-tap-8`).addEventListener(`click`, function() {
    var array = [];
    var innerContent = ``;
    var found = false;

    var number = document.getElementById(`input-number-8`).value;
    for (index = number; index > 0; index--) {
        array.push(index);
    }

    for (var index = 0; index < number; index++) {
        if (isPrime(array[index])) {
            innerContent += `${array[index]} is the first prime number element in the array.`;
            found = true;
            break;
        }
    }

    if (found === false) {
        innerContent += `-1`;
    }

    document.getElementById(`ket-qua-bai-tap-8`).innerHTML = innerContent;
});

/* BÀI TẬP 9 */
// TODO: Implement exercise 9.

/* BÀI TẬP 10 */
document.getElementById(`btn-bai-tap-10`).addEventListener(`click`, function() {
    var array = [];
    var evens = [];
    var odds = [];

    var number = document.getElementById(`input-number-10`).value;
    for (index = number; index > 0; index--) {
        array.push(index);
    }

    for (var index = 0; index < number; index++) {
        if (array[index] % 2 === 0) {
            evens.push(array[index]);
        } else {
            odds.push(array[index]);
        }
    }

    document.getElementById(`ket-qua-bai-tap-10`).innerHTML = `<b>Odds:</b> ${odds}.<br/><b>Evens:</b> ${evens}.`;
});